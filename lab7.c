#include <stdio.h>
struct student 
{
	char name[50];
	char section[50];
	char department[50];
	char result[10];
	int roll;
	int fees;
	float marks;
} s;
int main()
{
	printf("Enter information:\n");
	printf("Enter name: ");
	fgets(s.name, sizeof(s.name), stdin);
	printf("Enter section: ");
	fgets(s.section, sizeof(s.section), stdin);
	printf("Enter department: ");
	fgets(s.department, sizeof(s.department), stdin);
	printf("Enter roll number: ");
	scanf("%d", &s.roll);
	printf("Enter fees: ");
	scanf("%d", &s.fees);
	printf("Enter marks: ");
	scanf("%f", &s.marks);
	printf("Displaying Information:\n");
	printf("Name: ");
	printf("%s", s.name);
	printf("Section: ");
	printf("%s", s.section);
	printf("Department: ");
	printf("%s", s.department);
	printf("Roll number: %d\n", s.roll);
	printf("fees: %d\n", s.fees);
	printf("Marks: %.1f\n", s.marks);
	if(s.marks>35)
	{
		printf("Result: ");
		printf("%s pass", s.result);
	}
	else
	{
		printf("Result: ");
		printf("%s Fail", s.result);
	}
	return 0;
}