#include <stdio.h>
void swap_call_by_val(int, int);
void swap_call_by_ref(int *, int *);
int main()
{
	int a,b;
	printf("Enter Two  Numbers 'a' and 'b':" );
	scanf("%d%d",&a,&b);
	printf("\n Before Swaping a = %d and b = %d", a, b);
	swap_call_by_val(a, b);
	printf("\n After Swapping Through Call By Value Method, a = %d and b = %d", a, b);
	swap_call_by_ref(&a, &b);
	printf("\n After Swapping Through Call By Reference Method, a = %d and b = %d", a, b );
	return 0;
}
void swap_call_by_val(int p, int q)
{
	int temp;
	temp = p;
	p = q;
	q = temp;
}
void swap_call_by_ref(int *r, int *s)
{
	int temp;
	temp = *r;
	*r = *s;
	*s = temp;
}